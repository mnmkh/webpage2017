root = exports ? this

root.currentViewModel = null

# const
ESCAPED_FRAGMENT = "\\?_escaped_fragment_"

# google analysis tracking
# GATracking = () ->
#   set = (i,s,o,g,r,a,m) ->
#     i['GoogleAnalyticsObject']=r
#     i[r]= i[r] || ( () -> (i[r].q=i[r].q||[]).push(arguments) )
#     i[r].l=1*new Date()
#     a=s.createElement(o)
#     m=s.getElementsByTagName(o)[0]
#     a.async=1
#     a.src=g
#     m.parentNode.insertBefore(a,m)
#   set(window,document,'script','//www.google-analytics.com/analytics.js','ga')
#   ga 'create', 'UA-47361294-1', 'doyoulisp.org'
#   ga 'send', 'pageview'

# initialize
onReady = () ->
  # ViewModel initialize.
  root.currentViewModel = new BoardgameBlogViewModel
  ko.applyBindings root.currentViewModel, $('html')[0]
  # update article.
  root.currentViewModel.init window.location.pathname + window.location.search
  
$ ->
  # ESCAPED_FRAGMENTが存在する場合はrouting,bind処理不要のためデータを取得しない
  path = window.location.pathname + window.location.search
  if path.match ESCAPED_FRAGMENT 
    return        
  onReady()

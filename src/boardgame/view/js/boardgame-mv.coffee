root = exports ? this

# const 
ROOTPATH     = '/'
ARCHIVEQUERY = ROOTPATH + 'archive/'
ARCHIVE_TAG  = [
  name  : "全て",
  value : "*"
,
  name  : "レビュー",
  value : "レビュー"  
,
  name  : "ゲーム会",
  value : "ゲーム会"  
,
  name  : "まとめ",
  value : "まとめ"  
]
GAMELIST_QUERY = ROOTPATH + 'gamelist'
GAMELIST_ITEM_QUERY = GAMELIST_QUERY + '/guid'
GAMELIST_MENU = [
  name : "評価順",
  value: "gamelist-rating"
,
  name : "年代順",
  value: "gamelist-year-published"
]
RATING = [
  name : "お気に入り",
  value: 10
,
  name : "とても良い",
  value: 9
,
  name : "面白い",
  value: 8
,
  name : "良い",
  value: 7
,
  name : "その他",
  value: 0
,
  name : "未プレイ",
  value: -1
]

TOP_DESCRIPTION      = 'ボードゲームのことを書くサイト'
ARCHIVE_DESCRIPTION  = '記事一覧'
GAMELIST_DESCRIPTION = 'ゲーム評価'

# function
makeLocation = ( path ) ->
  location = window.location.protocol + '//' + window.location.host + path

separateObjectArray = (array , keyGetter) ->
  separateList = {}
  keyList = []
  for obj in array
    key = keyGetter(obj)
    if key of separateList
      separateList[key].push obj
    else
      separateList[key] = []
      separateList[key].push obj
      keyList.push key
  [separateList , keyList]

ratingMapper = (v) ->
  for r in RATING
    rate = v.rating
    if rate is null
      rate = -1
    if rate >= r.value
      return r.name

# index
registerIndex = (index , key , obj) ->
  if key of index
    index[key].push obj
  else
    index[key] = []
    index[key].push obj  

# page title binding
ko.bindingHandlers.pageTitle = 
  update: (element, valueAccessor) ->
    title = ko.utils.unwrapObservable valueAccessor()
    document.title = title

ko.virtualElements.allowedBindings.pageTitle = true

# ViewModel
class @BoardgameBlogViewModel
  constructor: () ->
  ################################################## 
  # variables 
  ##################################################
  # observable variables 
        @title = ko.observable '日曜ボードゲーム'
        @description = ko.observable TOP_DESCRIPTION
        @titleText = ko.computed (() -> return @title() + " : " + @description()), @
        @articleType = ko.observable 'gamelist'    
        @article = ko.observableArray []
        @tags = ko.observableArray ARCHIVE_TAG
        @archive = ko.observableArray []
        @gamelistMenu = ko.observableArray GAMELIST_MENU
        @gamelist = ko.observableArray []
        @gamelistViewMode = ko.observable 'gamelist-rating' #初期値はgamelist-rating
        @gamelistItem = ko.observable null
        # working variables     
        @blogItem = [] # データは日付順に並べる
        @nextPollPageNumber = 1
        @indexYearGamelist  = {}
        @indexRatingGamelist = {}
        @indexDesignerGamelist = {}    
        @indexMechanicGamelist = {}
        @indexFamilyGamelist = {}     
        ################################################## 
        # path - articleType mapping
        ##################################################
        @getArticleTypeFromPath = (path) ->
          if path.match GAMELIST_ITEM_QUERY
            'gamelist-item'        
          else if path.match GAMELIST_QUERY
            'gamelist'
          else
            'blog-item-summary'
        ################################################## 
        # initialize
        ##################################################        
        @init = (path) ->
          # 初期処理.アクセスされたpathのデータを取得してanimation無しで表示する。      
          vm = @
          type = @getArticleTypeFromPath path
          updateFunc = null
          if type is 'gamelist'
            path = 'data/gamelist.json'
            updateFunc = @gamelistUpdate
          else if type is 'gamelist-item'
            path = 'data/gamelist.json'
            updateFunc = @gamelistItemUpdate
          else
            path = 'data/gamelist.json'            
            updateFunc = @gamelistUpdate  
          @transition path, updateFunc, false
        @archiveInit = (data,event) ->
          # isotype filter
          $container = $('.isotope').isotope 
            itemSelector: '.element-item'
            layoutMode: 'fitRows'
        ################################################## 
        # page link action 
        ##################################################
        @gamelistLinkClick = (data, event) ->
          # jump gamelist page (with animation).
          @transition GAMELIST_QUERY, @gamelistUpdate, true
        @gamelistItemLinkClick = (guid,data,event) ->
          vm = @
          path = GAMELIST_ITEM_QUERY + guid + '/'
          item = _.find @gamelist() , (i) -> i.guid == guid
          if item
            $('article').fadeOut 'slow', () ->
              $('html, body').animate scrollTop: 0 , 0
              vm.gamelistItem item
              $('article').fadeIn 1500
          else
            @transition path, @gamelistItemUpdate, true
        ################################################## 
        # blog
        ##################################################      
        @pullBlogItem = (animation) ->
          # ページ単位でのblogデータ取得
          # animationはcustom bindingによりtemplate更新時に実行したいがtemplateのupdateがcallされない現象があるため妥協      
          vm = @
          unless vm.articleType() == 'blog-item-summary'
            return      
          if animation
            $('article').fadeOut 'slow', () ->
              $.ajax '/data/boardgame/view/page/' + @nextPollPageNumber + '/',
                type : 'GET',
                dataType: 'json',
                success: (data, textStatus, jqXHR) ->
                  vm.nextPollPageNumber++
                  vm.articleAppend data.data
                  vm.description TOP_DESCRIPTION
                  $('article').fadeIn 1500
          else 
            $.ajax '/data/boardgame/view/page/' + @nextPollPageNumber + '/',
              type : 'GET',
              dataType: 'json',
              success: (data, textStatus, jqXHR) ->
                vm.nextPollPageNumber++
                vm.articleAppend data.data
                vm.description TOP_DESCRIPTION            
        @transition = (path, updateFunc, animation) ->
          # ページデータを取得して遷移する
          vm = @
          if animation
            $('article').fadeOut 'slow', () ->      
              $.ajax path,
                type : 'GET',
                cache: true,
                dataType: 'json',
                success: (data, textStatus, jqXHR) ->
                  updateFunc vm, path, data
                  $('article').fadeIn 1500
          else
            $.ajax path,
              type : 'GET',
              cache: true,
              dataType: 'json',
              success: (data, textStatus, jqXHR) ->
                updateFunc vm, path, data
        @articleUpdate = (vm, path, itemList) ->
          vm.article.removeAll()
          $('html, body').animate scrollTop: 0 , 0
          vm.article.push.apply vm.article, itemList
          vm.blogItem.push.apply vm.blogItem, itemList
          _.sortBy vm.blogItem , (d) -> return d.date
        @articleAppend = (itemList) ->
          for item in itemList
            unless (_.find @article , (i) -> i.id == item.id)
              @article.push item
              @blogItem.push item
          _.sortBy @blogItem , (d) -> return d.date
        # ページ末尾到達時の処理
        @endPageAction = () ->
          if @articleType() == 'blog-item-summary'
            @pullBlogItem false
        @getRecommendData = (item) ->
          item.recommend = ko.observableArray []      
          $.ajax '/data/boardgame/recommend/' + item.id + '/',
            type : 'GET',
            cache: true,
            dataType: 'json',
            success: (data, textStatus, jqXHR) ->
              item.recommend.removeAll()
              item.recommend.push.apply item.recommend, data.data
          return item.recommend
        @getRecommendThumbnail = (thumbnail) ->
          unless thumbnail
            return ''
          thumbnail.replace /_z.jpg/, '_m.jpg'
        @getShowDate = (date) ->
          unless date
            return ''
          date.replace /T(.*)/, ''
        ################################################## 
        # gamelist
        ##################################################        
        @gamelistUpdate = (vm, path, gamelist) ->
          vm.gamelist.removeAll()
          vm.gamelist.push.apply vm.gamelist, gamelist
          vm.createGamelistIndex()      
          vm.description GAMELIST_DESCRIPTION      
        @gamelistViewModeChange = (mode, data, event) ->
          @gamelistViewMode mode
        @yearPublishGamelist = () ->    
          @indexYearGamelist  = {}    
          ## レーティングもないゲームは除外する
          useGamelist = _.filter @gamelist() , (i) -> (i.rating isnt null)
          ## ゲームを年代別に分ける
          for item in useGamelist
            registerIndex @indexYearGamelist, item.yearpublished, item
        # 表示用に並び替え
          yearList = _.sortBy Object.keys @indexYearGamelist , (y) -> return y      
          sortedGameList = []
          for year in yearList
            yearGameList = {}
            yearGameList.name = year
            yearGameList.gamelist = []
            yearGameList.gamelist.push.apply yearGameList.gamelist, @indexYearGamelist[year]
            sortedGameList.push yearGameList
          sortedGameList
        @ratingGamelist = () ->
          #ゲームをレーティングで分ける
          useGamelist = _.filter @gamelist() , (i) -> (i.own isnt "f") or (i.rating isnt null)
          [separateGamelist, useRatingList] = separateObjectArray useGamelist , ratingMapper
          # 表示用に並び替え
          sortedGameList = []
          for rating in RATING
            oneListGroup = {}
            oneListGroup.name = rating.name
            oneListGroup.gamelist = []
            if rating.name of separateGamelist
              oneListGroup.gamelist.push.apply  oneListGroup.gamelist, separateGamelist[rating.name]
              sortedGameList.push oneListGroup
          sortedGameList                     
        @getSquareThumbnailLink = (thumbnail) ->
          unless thumbnail
            return ""
          thumbnail.replace /_t.jpg/, '_sq.jpg'
        @getRatingBorder = (rating) ->
          unless rating
            return ""
          if rating >= 9
            return "gold-border"
          else if rating >= 8
            return "grey-border"
          else if rating >= 6
            return "bronze-border"
          else
            return ""
        @createGamelistIndex = () ->
    #      @clearGameListIndex()
          # 所持しておらずレーティングもないゲームは除外する      
    #      useGamelist = _.filter @gamelist() , (i) -> (i.own isnt "f") or (i.rating isnt null)
#          console.time('createIndex')
    #      for item in useGamelist
            # rating
    #        ratingName = ratingMapper item
    #        registerIndex @indexRatingGamelist, ratingName, item
            # year
    #        registerIndex @indexYearGamelist, item.yearpublished, item
            # designer
    #        designerList = @parseStrList item.designer
    #        if designerList isnt null
    #          for designer in designerList
    #            registerIndex @indexDesignerGamelist, designer, item
    #        # Mechanic
    #        mechanicList = @parseStrList item.boardgamemechanic
    #        if mechanicList isnt null
    #          for mechanic in mechanicList        
    #            registerIndex @indexMechanicGamelist, mechanic, item
    #        # family
    #        familyList   = @parseStrList item.boardgamefamily
    #        if familyList isnt null
    #          for family in familyList                    
    #            registerIndex @indexFamilyGamelist, family, item
#          console.timeEnd('createIndex') 
          return
        @clearGameListIndex = () ->
          @indexYearGamelist  = {}
          @indexRatingGamelist = {}
          @indexDesignerGamelist = {}    
          @indexMechanicGamelist = {}
          @indexFamilyGamelist = {}     
        ################################################## 
        # gamelist
        ##################################################
        @gamelistItemUpdate = (vm, path, gamelist)  ->
          # now return all itemdata
          match = path.match GAMELIST_ITEM_QUERY + "([0-9]+)"
          guid = match[1]
          vm.gamelist.removeAll()
          vm.gamelist.push.apply vm.gamelist, gamelist
          vm.createGamelistIndex()
          item = _.find vm.gamelist() , (i) -> i.guid == guid      
          vm.gamelistItem item
        @makeBggLink  = (guid)    ->
          'http://boardgamegeek.com/boardgame/' + guid + '/'
        @parseStrList = (strList) ->
          data = XRegExp.forEach strList,
                                 XRegExp('"([\\p{L}|\\s|/]+)"'),
                                 (match, i) -> this.push match[1],
                                 []
        @joinStrList = (strList) ->
          if strList is null
            return strList      
          data = @parseStrList strList
          data.join(',')      
        @rateToRank = (r) ->
          ob = {}
          ob['rating'] = r
          ratingMapper ob

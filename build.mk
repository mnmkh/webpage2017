## variables
PWD:=`pwd`
DIST_DIR:=$(PWD)/dist
DIST_HTML_DIR:=$(DIST_DIR)/

## boardgame 
BG_SRC_DIR:=$(PWD)/src/boardgame/view
BG_SRC_TMPL_DIR:=$(BG_SRC_DIR)/tmpl/
BG_DIST_DIR:=$(DIST_DIR)/

BG_SRC_ARCHIVE_DIR:=$(PWD)/src/boardgame/view/
#BG_SRC_ARCHIVE_TMPL_DIR:=$(BG_SRC_DIR)/tmpl/
BG_DIST_ARCHIVE_DIR:=$(DIST_DIR)/archive/

JSON_DIST_DIR=$(DIST_DIR)/data/
JSON_DIST_GAMELIST_FILE=$(JSON_DIST_DIR)/gamelist.json

## source
all: build
build: html grunt json
html : html-bg
json : json-bg
grunt: grunt-bg

# boardgame service
bg   : html-bg grunt-bg json-bg
html-bg: 
	# boardgame html components build	
	@echo "> Generate html files from template files..."
	@./bin/gen-html.lisp $(BG_SRC_TMPL_DIR) $(BG_DIST_DIR)
#	@./bin/gen-html.lisp $(BG_SRC_ARCHIVE_TMPL_DIR) $(BG_DIST_ARCHIVE_DIR)
	@echo "> done."

grunt-bg:
	# grunt build
	@echo "> Generate js , css , image files..."
	@cd $(BG_SRC_DIR) ; npm install ; grunt
	@echo "> done."

json-bg:
	# json data
	@echo "> Generate json files..."
	@mkdir -p $(JSON_DIST_DIR)
	@./bin/bgg-db-to-json.lisp data/bgg.db $(JSON_DIST_GAMELIST_FILE)
	@echo "> done."

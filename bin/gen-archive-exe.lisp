#!/usr/local/bin/sbcl --script
;;;;
;;;; Author: mnmkh
;;;; generate html script
;;;; 

(load #p"~/.sbclrc")

;;; add libpath
(load #p"./bin/load-lib.lisp")

;; variable
(defvar *template-directory* (second sb-ext:*posix-argv*))
(defvar *target-directory* (third sb-ext:*posix-argv*))

(defun usage ()
  (format t "[Usage] gen-archive TEMPLATE-DIREDTORY TARGET-DIRECTORY (EVAL_PACKAGE) ~%"))

;(create-abs-path *lib-directory*)


;(ql:quickload "skip")
;(add-libpath! #p"./lib/app-db/")
;(let ((*standard-output* (make-string-output-stream)))
;  (ql:quickload "app-db"))

;(when (or (< (length sb-ext:*posix-argv*) 3) 
;	  (> (length sb-ext:*posix-argv*) 4))
 ; (usage)
  ;(exit))

;; program
(ql:quickload "gen-archive")

(gen-archive:run)

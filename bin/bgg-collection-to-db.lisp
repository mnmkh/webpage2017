#!/usr/local/bin/sbcl --script
;;;;
;;;; Author: mnmkh
;;;; 
(load #p"~/.sbclrc")

(load #p"./bin/load-lib.lisp")

(defun usage ()
  (format t "[Usage] bgg-collection-to-db username output ~%"))

(defun make-keyword (name) (values (intern name "KEYWORD")))

(when (or (< (length sb-ext:*posix-argv*) 2) 
	  (> (length sb-ext:*posix-argv*) 3))
  (usage)
  (exit))

;; variable
(defvar *username* (second sb-ext:*posix-argv*))
(defvar *output-db* (third sb-ext:*posix-argv*))

;;
;; main program.
;;
(format t "[template directory]: ~a ~%" *username*)
(format t "[target directory]: ~a ~%" *output-db*)

;; gennerate html files.
(ql:quickload :cl-bgg)
(ql:quickload :app-db)
(ql:quickload :clsql)
(ql:quickload :clsql-sqlite3)

(defparameter *connection*
  (clsql:connect '("data/bgg.db")
		 :if-exists :old :database-type :sqlite3))

(app-db:setup-bgg-db *connection*)

; get collection  
(let ((items-sxml (cl-bgg:collection *username*)))
  (app-db:push-collections-to-db *username* items-sxml  :db *connection*))

(let ((collections (app-db:find-collections *username* :db *connection*)))
  (loop for c in collections 
       do (let ((guid (app-db:guid-of c)))
	    (format t "~a " guid)	    	    
	    (skip:aif (app-db:find-boardgame guid :db *connection*)
		      (format t " skip. ~%")
		      (progn 
			(format t " get . ~%")
			(app-db:push-boardgame-to-db-from-guid guid :db *connection*))))))

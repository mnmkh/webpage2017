(in-package :cl-user)

(asdf:defsystem :gen-archive
    :name "gen-archive"
    :description "gen-archive."
    :version "0.1"
    :author "mnmkh"
    :serial T
    :encoding :utf-8
    :components 
              (;(:file "packages")
               (:file "gen-archive"))
    :depends-on (:skip
		 :clsql
		 :clsql-sqlite3
		 :app-db))

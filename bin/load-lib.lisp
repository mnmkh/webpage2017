(ql:quickload "cl-fad")

(defun create-abs-path (path)
  (merge-pathnames path *DEFAULT-PATHNAME-DEFAULTS*))

(defun add-libpath! (path)
  (pushnew path asdf:*central-registry*))

(defvar *lib-directory* "lib")
(map nil (lambda (path) 
           (when (cl-fad:directory-pathname-p path)
             (add-libpath! (pathname path))))
     (cl-fad:list-directory *lib-directory*))

(defvar *bin-directory* "bin")
(map nil (lambda (path) 
           (when (cl-fad:directory-pathname-p path)
             (add-libpath! (pathname path))))
     (cl-fad:list-directory *bin-directory*))

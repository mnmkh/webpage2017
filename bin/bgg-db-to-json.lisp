#!/usr/local/bin/sbcl --script
;;;;
;;;; Author: mnmkh
;;;; 
(load #p"~/.sbclrc")

(load #p"./bin/load-lib.lisp")

(defun usage ()
  (format t "[Usage] bgg-db-to-json db output_file ~%"))

(when (or (< (length sb-ext:*posix-argv*) 2) 
	  (> (length sb-ext:*posix-argv*) 3))
  (usage)
  (exit))

(defvar *db* (second sb-ext:*posix-argv*))
(defvar *output-file* (third sb-ext:*posix-argv*))

;; variable
(ql:quickload :app-db)
(ql:quickload :alexandria)
(ql:quickload :cl-json)

(defparameter *connection*
  (clsql:connect (list *db*)
;		 '("data/bgg.db")
		 :if-exists :old :database-type :sqlite3))

(defun make-gamelist-data ()
  (let* ((items (app-db:find-boardgame-for-year-published-view :db *connection*)))
    (loop for item in items 
	 collect (alexandria:alist-hash-table (map 'list #'(lambda (k v) `(,k . ,v))    
						   '(:guid :name :jpname :yearpublished :thumbnail :designer :boardgamemechanic :boardgamefamily :rating :own :wanttobuy :wanttoplay)
						   item)))))

(with-open-file (out *output-file*  :direction :output :if-exists :supersede :if-does-not-exist :create)
  (format out "~a ~%" (json:encode-json-to-string (make-gamelist-data))))


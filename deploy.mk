## variables
PWD:=`pwd`
DEPLOY_DIR:=$(PWD)/deploy

## source
all: sbcl

sbcl:
	mkdir -p ${DEPLOY_DIR}
	cd ${DEPLOY_DIR}
	curl https://jaist.dl.sourceforge.net/project/sbcl/sbcl/1.4.2/sbcl-1.4.2-x86-64-linux-binary.tar.bz2 -o sbcl-1.4.2-x86-64-linux-binary.tar.bz2
#https://downloads.sourceforge.net/project/sbcl/sbcl/1.4.2/sbcl-1.4.2-x86-64-linux-binary.tar.bz2?r=http%3A%2F%2Fwww.sbcl.org%2Fplatform-table.html&ts=1514790055&use_mirror=jaist -o sbcl-1.4.2-x86-64-linux-binary.tar.bz2
	tar xf sbcl-1.4.2-x86-64-linux-binary.tar.bz2
	cd sbcl-1.4.2-x86-64-linux
	sh install.sh

clone:
	git clone git@bitbucket.org:mnmkh/cl-ws.git lib/cl-ws
	git clone git@bitbucket.org:mnmkh/cl-bgg.git lib/cl-bgg
	git clone git@bitbucket.org:mnmkh/skip.git lib/skip

(function() {
  var ARCHIVEQUERY, ARCHIVE_DESCRIPTION, ARCHIVE_TAG, GAMELIST_DESCRIPTION, GAMELIST_ITEM_QUERY, GAMELIST_MENU, GAMELIST_QUERY, RATING, ROOTPATH, TOP_DESCRIPTION, makeLocation, ratingMapper, registerIndex, root, separateObjectArray;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  ROOTPATH = '/';

  ARCHIVEQUERY = ROOTPATH + 'archive/';

  ARCHIVE_TAG = [
    {
      name: "全て",
      value: "*"
    }, {
      name: "レビュー",
      value: "レビュー"
    }, {
      name: "ゲーム会",
      value: "ゲーム会"
    }, {
      name: "まとめ",
      value: "まとめ"
    }
  ];

  GAMELIST_QUERY = ROOTPATH + 'gamelist';

  GAMELIST_ITEM_QUERY = GAMELIST_QUERY + '/guid';

  GAMELIST_MENU = [
    {
      name: "評価順",
      value: "gamelist-rating"
    }, {
      name: "年代順",
      value: "gamelist-year-published"
    }
  ];

  RATING = [
    {
      name: "お気に入り",
      value: 10
    }, {
      name: "とても良い",
      value: 9
    }, {
      name: "面白い",
      value: 8
    }, {
      name: "良い",
      value: 7
    }, {
      name: "その他",
      value: 0
    }, {
      name: "未プレイ",
      value: -1
    }
  ];

  TOP_DESCRIPTION = 'ボードゲームのことを書くサイト';

  ARCHIVE_DESCRIPTION = '記事一覧';

  GAMELIST_DESCRIPTION = 'ゲーム評価';

  makeLocation = function(path) {
    var location;
    return location = window.location.protocol + '//' + window.location.host + path;
  };

  separateObjectArray = function(array, keyGetter) {
    var key, keyList, obj, separateList, _i, _len;
    separateList = {};
    keyList = [];
    for (_i = 0, _len = array.length; _i < _len; _i++) {
      obj = array[_i];
      key = keyGetter(obj);
      if (key in separateList) {
        separateList[key].push(obj);
      } else {
        separateList[key] = [];
        separateList[key].push(obj);
        keyList.push(key);
      }
    }
    return [separateList, keyList];
  };

  ratingMapper = function(v) {
    var r, rate, _i, _len;
    for (_i = 0, _len = RATING.length; _i < _len; _i++) {
      r = RATING[_i];
      rate = v.rating;
      if (rate === null) {
        rate = -1;
      }
      if (rate >= r.value) {
        return r.name;
      }
    }
  };

  registerIndex = function(index, key, obj) {
    if (key in index) {
      return index[key].push(obj);
    } else {
      index[key] = [];
      return index[key].push(obj);
    }
  };

  ko.bindingHandlers.pageTitle = {
    update: function(element, valueAccessor) {
      var title;
      title = ko.utils.unwrapObservable(valueAccessor());
      return document.title = title;
    }
  };

  ko.virtualElements.allowedBindings.pageTitle = true;

  this.BoardgameBlogViewModel = (function() {

    function BoardgameBlogViewModel() {
      this.title = ko.observable('日曜ボードゲーム');
      this.description = ko.observable(TOP_DESCRIPTION);
      this.titleText = ko.computed((function() {
        return this.title() + " : " + this.description();
      }), this);
      this.articleType = ko.observable('gamelist');
      this.article = ko.observableArray([]);
      this.tags = ko.observableArray(ARCHIVE_TAG);
      this.archive = ko.observableArray([]);
      this.gamelistMenu = ko.observableArray(GAMELIST_MENU);
      this.gamelist = ko.observableArray([]);
      this.gamelistViewMode = ko.observable('gamelist-rating');
      this.gamelistItem = ko.observable(null);
      this.blogItem = [];
      this.nextPollPageNumber = 1;
      this.indexYearGamelist = {};
      this.indexRatingGamelist = {};
      this.indexDesignerGamelist = {};
      this.indexMechanicGamelist = {};
      this.indexFamilyGamelist = {};
      this.getArticleTypeFromPath = function(path) {
        if (path.match(GAMELIST_ITEM_QUERY)) {
          return 'gamelist-item';
        } else if (path.match(GAMELIST_QUERY)) {
          return 'gamelist';
        } else {
          return 'blog-item-summary';
        }
      };
      this.init = function(path) {
        var type, updateFunc, vm;
        vm = this;
        type = this.getArticleTypeFromPath(path);
        updateFunc = null;
        if (type === 'gamelist') {
          path = 'data/gamelist.json';
          updateFunc = this.gamelistUpdate;
        } else if (type === 'gamelist-item') {
          path = 'data/gamelist.json';
          updateFunc = this.gamelistItemUpdate;
        } else {
          path = 'data/gamelist.json';
          updateFunc = this.gamelistUpdate;
        }
        return this.transition(path, updateFunc, false);
      };
      this.archiveInit = function(data, event) {
        var $container;
        return $container = $('.isotope').isotope({
          itemSelector: '.element-item',
          layoutMode: 'fitRows'
        });
      };
      this.gamelistLinkClick = function(data, event) {
        return this.transition(GAMELIST_QUERY, this.gamelistUpdate, true);
      };
      this.gamelistItemLinkClick = function(guid, data, event) {
        var item, path, vm;
        vm = this;
        path = GAMELIST_ITEM_QUERY + guid + '/';
        item = _.find(this.gamelist(), function(i) {
          return i.guid === guid;
        });
        if (item) {
          return $('article').fadeOut('slow', function() {
            $('html, body').animate({
              scrollTop: 0
            }, 0);
            vm.gamelistItem(item);
            return $('article').fadeIn(1500);
          });
        } else {
          return this.transition(path, this.gamelistItemUpdate, true);
        }
      };
      this.pullBlogItem = function(animation) {
        var vm;
        vm = this;
        if (vm.articleType() !== 'blog-item-summary') {
          return;
        }
        if (animation) {
          return $('article').fadeOut('slow', function() {
            return $.ajax('/data/boardgame/view/page/' + this.nextPollPageNumber + '/', {
              type: 'GET',
              dataType: 'json',
              success: function(data, textStatus, jqXHR) {
                vm.nextPollPageNumber++;
                vm.articleAppend(data.data);
                vm.description(TOP_DESCRIPTION);
                return $('article').fadeIn(1500);
              }
            });
          });
        } else {
          return $.ajax('/data/boardgame/view/page/' + this.nextPollPageNumber + '/', {
            type: 'GET',
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
              vm.nextPollPageNumber++;
              vm.articleAppend(data.data);
              return vm.description(TOP_DESCRIPTION);
            }
          });
        }
      };
      this.transition = function(path, updateFunc, animation) {
        var vm;
        vm = this;
        if (animation) {
          return $('article').fadeOut('slow', function() {
            return $.ajax(path, {
              type: 'GET',
              cache: true,
              dataType: 'json',
              success: function(data, textStatus, jqXHR) {
                updateFunc(vm, path, data);
                return $('article').fadeIn(1500);
              }
            });
          });
        } else {
          return $.ajax(path, {
            type: 'GET',
            cache: true,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
              return updateFunc(vm, path, data);
            }
          });
        }
      };
      this.articleUpdate = function(vm, path, itemList) {
        vm.article.removeAll();
        $('html, body').animate({
          scrollTop: 0
        }, 0);
        vm.article.push.apply(vm.article, itemList);
        vm.blogItem.push.apply(vm.blogItem, itemList);
        return _.sortBy(vm.blogItem, function(d) {
          return d.date;
        });
      };
      this.articleAppend = function(itemList) {
        var item, _i, _len;
        for (_i = 0, _len = itemList.length; _i < _len; _i++) {
          item = itemList[_i];
          if (!(_.find(this.article, function(i) {
            return i.id === item.id;
          }))) {
            this.article.push(item);
            this.blogItem.push(item);
          }
        }
        return _.sortBy(this.blogItem, function(d) {
          return d.date;
        });
      };
      this.endPageAction = function() {
        if (this.articleType() === 'blog-item-summary') {
          return this.pullBlogItem(false);
        }
      };
      this.getRecommendData = function(item) {
        item.recommend = ko.observableArray([]);
        $.ajax('/data/boardgame/recommend/' + item.id + '/', {
          type: 'GET',
          cache: true,
          dataType: 'json',
          success: function(data, textStatus, jqXHR) {
            item.recommend.removeAll();
            return item.recommend.push.apply(item.recommend, data.data);
          }
        });
        return item.recommend;
      };
      this.getRecommendThumbnail = function(thumbnail) {
        if (!thumbnail) {
          return '';
        }
        return thumbnail.replace(/_z.jpg/, '_m.jpg');
      };
      this.getShowDate = function(date) {
        if (!date) {
          return '';
        }
        return date.replace(/T(.*)/, '');
      };
      this.gamelistUpdate = function(vm, path, gamelist) {
        vm.gamelist.removeAll();
        vm.gamelist.push.apply(vm.gamelist, gamelist);
        vm.createGamelistIndex();
        return vm.description(GAMELIST_DESCRIPTION);
      };
      this.gamelistViewModeChange = function(mode, data, event) {
        return this.gamelistViewMode(mode);
      };
      this.yearPublishGamelist = function() {
        var item, sortedGameList, useGamelist, year, yearGameList, yearList, _i, _j, _len, _len1;
        this.indexYearGamelist = {};
        useGamelist = _.filter(this.gamelist(), function(i) {
          return i.rating !== null;
        });
        for (_i = 0, _len = useGamelist.length; _i < _len; _i++) {
          item = useGamelist[_i];
          registerIndex(this.indexYearGamelist, item.yearpublished, item);
        }
        yearList = _.sortBy(Object.keys(this.indexYearGamelist, function(y) {
          return y;
        }));
        sortedGameList = [];
        for (_j = 0, _len1 = yearList.length; _j < _len1; _j++) {
          year = yearList[_j];
          yearGameList = {};
          yearGameList.name = year;
          yearGameList.gamelist = [];
          yearGameList.gamelist.push.apply(yearGameList.gamelist, this.indexYearGamelist[year]);
          sortedGameList.push(yearGameList);
        }
        return sortedGameList;
      };
      this.ratingGamelist = function() {
        var oneListGroup, rating, separateGamelist, sortedGameList, useGamelist, useRatingList, _i, _len, _ref;
        useGamelist = _.filter(this.gamelist(), function(i) {
          return (i.own !== "f") || (i.rating !== null);
        });
        _ref = separateObjectArray(useGamelist, ratingMapper), separateGamelist = _ref[0], useRatingList = _ref[1];
        sortedGameList = [];
        for (_i = 0, _len = RATING.length; _i < _len; _i++) {
          rating = RATING[_i];
          oneListGroup = {};
          oneListGroup.name = rating.name;
          oneListGroup.gamelist = [];
          if (rating.name in separateGamelist) {
            oneListGroup.gamelist.push.apply(oneListGroup.gamelist, separateGamelist[rating.name]);
            sortedGameList.push(oneListGroup);
          }
        }
        return sortedGameList;
      };
      this.getSquareThumbnailLink = function(thumbnail) {
        if (!thumbnail) {
          return "";
        }
        return thumbnail.replace(/_t.jpg/, '_sq.jpg');
      };
      this.getRatingBorder = function(rating) {
        if (!rating) {
          return "";
        }
        if (rating >= 9) {
          return "gold-border";
        } else if (rating >= 8) {
          return "grey-border";
        } else if (rating >= 6) {
          return "bronze-border";
        } else {
          return "";
        }
      };
      this.createGamelistIndex = function() {};
      this.clearGameListIndex = function() {
        this.indexYearGamelist = {};
        this.indexRatingGamelist = {};
        this.indexDesignerGamelist = {};
        this.indexMechanicGamelist = {};
        return this.indexFamilyGamelist = {};
      };
      this.gamelistItemUpdate = function(vm, path, gamelist) {
        var guid, item, match;
        match = path.match(GAMELIST_ITEM_QUERY + "([0-9]+)");
        guid = match[1];
        vm.gamelist.removeAll();
        vm.gamelist.push.apply(vm.gamelist, gamelist);
        vm.createGamelistIndex();
        item = _.find(vm.gamelist(), function(i) {
          return i.guid === guid;
        });
        return vm.gamelistItem(item);
      };
      this.makeBggLink = function(guid) {
        return 'http://boardgamegeek.com/boardgame/' + guid + '/';
      };
      this.parseStrList = function(strList) {
        var data;
        return data = XRegExp.forEach(strList, XRegExp('"([\\p{L}|\\s|/]+)"'), function(match, i) {
          return this.push(match[1]);
        }, []);
      };
      this.joinStrList = function(strList) {
        var data;
        if (strList === null) {
          return strList;
        }
        data = this.parseStrList(strList);
        return data.join(',');
      };
      this.rateToRank = function(r) {
        var ob;
        ob = {};
        ob['rating'] = r;
        return ratingMapper(ob);
      };
    }

    return BoardgameBlogViewModel;

  })();

}).call(this);

(function() {
  var ESCAPED_FRAGMENT, onReady, root;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  root.currentViewModel = null;

  ESCAPED_FRAGMENT = "\\?_escaped_fragment_";

  onReady = function() {
    root.currentViewModel = new BoardgameBlogViewModel;
    ko.applyBindings(root.currentViewModel, $('html')[0]);
    return root.currentViewModel.init(window.location.pathname + window.location.search);
  };

  $(function() {
    var path;
    path = window.location.pathname + window.location.search;
    if (path.match(ESCAPED_FRAGMENT)) {
      return;
    }
    return onReady();
  });

}).call(this);

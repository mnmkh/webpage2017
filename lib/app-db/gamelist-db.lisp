;;;; boardgame service database.
(in-package :app-db)

(annot:enable-annot-syntax)

;;; view
@export-class
(def-view-class <bgg-boardgame> ()
  ((guid :accessor guid-of
	   :db-kind :key
	   :db-constraints (:not-null :unique)
	   :type (varchar 8)
	   :initarg :guid)
   (update-time :accessor update-time-of
	      :db-constraints :not-null
	      :type wall-time
	      :initarg :update-time)
   (yearpublished :accessor yearpublished-of
		  :type (varchar 8)
		  :initarg :yearpublished)
   (minplayers :accessor minplayers-of
	       :type (varchar 4)
	       :initarg :minplayers)
   (maxplayers :accessor maxplayers-of
	       :type (varchar 4)
	       :initarg :maxplayers)
   (playingtime :accessor playingtime-of
		:type (varchar 8)
		:initarg :playingtime)
   (age :accessor age-of
	:type (varchar 4)
	:initarg :age)
   (name :accessor name-of
	 :type (varchar 256)
	 :initarg :name)
   (thumbnail :accessor thumbnail-of
	      :type (varchar 128)
	      :initarg :thumbnail)
   (image :accessor image-of
	  :type (varchar 128)
	  :initarg :image)
   (description :accessor description-of
		:type (varchar 32768)
		:initarg :description)
   (boardgamemechanic :accessor boardgamemechanic-of
		      :type list
		      :initarg :boardgamemechanic)
   (boardgamecategory :accessor boardgamecategory-of
		      :type list
		      :initarg :boardgamecategory)
   (boardgameversion :accessor boardgameversion-of
		     :type list
		     :initarg :boardgameversion)
   (boardgamedesigner :accessor boardgamedesigner-of
		      :type list
		      :initarg :boardgamedesigner)
   (boardgamepublisher :accessor boardgamepublisher-of
		       :type list
		       :initarg :boardgamepublisher)
   (boardgamefamily :accessor boardgamefamily-of
		      :type list
		      :initarg :boardgamefamily)))

;;; for collection cache class.
@export-class
(def-view-class <bgg-collection> ()
  ((username :accessor username-of
	     :db-kind :key
	     :db-constraints :not-null
	     :type (varchar 32)
	     :initarg :username)
   (guid :accessor guid-of
	 :db-kind :key
	 :db-constraints :not-null
	 :type (varchar 8)
	 :initarg :guid)
   (own :accessor ownp
	:type boolean
	:initarg :own)
   (rating :accessor rating-of
	   :type integer
	   :initarg :rating)
   (comment :accessor comment-of
	    :type (varchar 8192)
	    :initarg :comment)
   (wanttobuy :accessor wanttobuyp
	      :type boolean
	      :initarg :wanttobuy)
   (wanttoplay :accessor wanttoplayp
	       :type boolean
	       :initarg :wanttoplay)))

;;; boardgame tag
(def-view-class <guid-tag-map> ()
  ((guid :accessor guid-of
           :db-constraints :not-null
           :type integer
           :initarg :guid)
   (tagid  :accessor tagid-of
           :db-constraints :not-null
           :type integer
           :initarg :tagid))
  (:base-table <guid-tag-map>))

(def-view-class <bg-designer-tag> (<tag>) ())
(def-view-class <bg-designer-tag-map> (<guid-tag-map>) ())
(def-view-class <bg-mechanic-tag> (<tag>) ())
(def-view-class <bg-mechanic-tag-map> (<guid-tag-map>) ())
(def-view-class <bg-family-tag> (<tag>) ())
(def-view-class <bg-family-tag-map> (<guid-tag-map>) ())

;;; temporary for message class.
@export-class
(def-view-class <bgg-newgame> ()
  ((id :accessor id-of
       :db-kind :key
       :db-constraints (:not-null :unique)
       :type integer
       :initarg :id)
   (guids :accessor guids-of
          :type list
	  :initarg :guids)))

;;; jp name table class.
@export-class
(def-view-class <bgg-boardgame-jpname> ()
  ((guid :accessor guid-of
	 :db-kind :key
	 :db-constraints (:not-null :unique)
	 :type (varchar 8)
	 :initarg :guid)
   (jpname :accessor jpname-of
	   :db-constraints (:not-null :unique)
	   :type (varchar 256)
	   :initarg :jpname)))

@export
(defun make-bgg-boardgame-jpname (guid jpname)
  (make-instance '<bgg-boardgame-jpname>
		 :guid guid
		 :jpname jpname))

(defvar *bgg-db*)

@export
(defun setup-bgg-db (db)
  ;; create table
  (when db
    (setf *bgg-db* db)
    (unless (table-exists-p '<bgg-boardgame> :database db)
      (create-view-from-class '<bgg-boardgame> :database db))
    (unless (table-exists-p '<bgg-newgame> :database db)
      (create-view-from-class '<bgg-newgame> :database db)
      (push-guids-to-db nil :db db))
    (unless (table-exists-p '<bgg-collection> :database db)
      (create-view-from-class '<bgg-collection> :database db))
    (unless (table-exists-p '<bgg-boardgame-jpname>)
      (create-view-from-class '<bgg-boardgame-jpname> :database db))
    ;; create tag table
    (map nil (lambda (name)
               (unless (table-exists-p name :database db)
                 (create-view-from-class name :database db)))
        '( <bg-designer-tag>
	   <bg-designer-tag-map>
	   <bg-mechanic-tag>
	   <bg-mechanic-tag-map>
	   <bg-family-tag>
	   <bg-family-tag-map>))
    ;; create sequence
    (map nil (lambda (name)
	       (unless (sequence-exists-p name :database db)
		 (create-sequence name :database db)))
	'( <bg-designer-tag-counter>
	   <bg-mechanic-tag-counter>
	   <bg-family-tag-counter> ))))

@export
(defun make-boardgame (bg-xmls)
  (make-instance '<bgg-boardgame>
		 :guid (cl-bgg:guid-of bg-xmls)
		 :update-time (get-time)
		 :yearpublished (cl-bgg:yearpublished-of bg-xmls)
		 :minplayers (cl-bgg:minplayers-of bg-xmls)
		 :maxplayers (cl-bgg:maxplayers-of bg-xmls)
		 :playingtime (cl-bgg:playingtime-of bg-xmls)
		 :age (cl-bgg:age-of bg-xmls)
		 :name (cl-bgg:name-of bg-xmls)
		 :thumbnail (cl-bgg:thumbnail-of bg-xmls)
		 :image (cl-bgg:image-of bg-xmls)
		 :description (cl-bgg:description-of bg-xmls)
		 :boardgamemechanic (cl-bgg:boardgamemechanic-of bg-xmls)
		 :boardgamecategory (cl-bgg:boardgamecategory-of bg-xmls)
		 :boardgamefamily (cl-bgg:boardgamefamily-of bg-xmls)
		 :boardgameversion (cl-bgg:boardgameversion-of bg-xmls)
		 :boardgamedesigner (cl-bgg:boardgamedesigner-of bg-xmls)
		 :boardgamepublisher (cl-bgg:boardgamepublisher-of bg-xmls)))

@export
(defun update-boardgame (instance bg-xmls)
  (setf (update-time-of instance) (get-time)
	(yearpublished-of instance) (cl-bgg:yearpublished-of bg-xmls)
	(minplayers-of instance) (cl-bgg:minplayers-of bg-xmls)
	(maxplayers-of instance) (cl-bgg:maxplayers-of bg-xmls)
	(playingtime-of instance) (cl-bgg:playingtime-of bg-xmls)
	(age-of instance) (cl-bgg:age-of bg-xmls)
	(name-of instance) (cl-bgg:name-of bg-xmls)
	(thumbnail-of instance) (cl-bgg:thumbnail-of bg-xmls)
	(image-of instance) (cl-bgg:image-of bg-xmls)
	(description-of instance) (cl-bgg:description-of bg-xmls)
	(boardgamemechanic-of instance) (cl-bgg:boardgamemechanic-of bg-xmls)
	(boardgamecategory-of instance) (cl-bgg:boardgamecategory-of bg-xmls)
	(boardgamefamily-of instance) (cl-bgg:boardgamefamily-of bg-xmls)
	(boardgameversion-of instance) (cl-bgg:boardgameversion-of bg-xmls)
	(boardgamedesigner-of instance) (cl-bgg:boardgamedesigner-of bg-xmls)
	(boardgamepublisher-of instance) (cl-bgg:boardgamepublisher-of bg-xmls)))

@export
(defun push-boardgame-to-db (bg-xmls &key (db *bgg-db*))
  (when bg-xmls
    (let* ((guid (cl-bgg:guid-of bg-xmls))
	   (bg-instance (car (find-boardgame guid :db db))))
    (if bg-instance 
	(update-boardgame bg-instance bg-xmls)
	(setf bg-instance (make-boardgame bg-xmls)))
    (with-transaction ()
      (update-records-from-instance bg-instance :database db)))))

@export
(defun push-boardgame-to-db-from-guid (guid &key (db *bgg-db*))
  (when guid
    (awhen (cl-bgg:boardgame guid)
      (push-boardgame-to-db it))))

@export
(defun truevaluep (value) (string= value "1"))

@export
(defun convert-rating-value (value)
  (whenbind rating (ppcre:scan-to-strings "(\\d+|\\d+.\\d)" value)
    (parse-integer rating)))

@export
(defun make-collection (username item-sxml)
  (make-instance '<bgg-collection>
		 :username username
		 :guid (cl-bgg:item-guid-of item-sxml)
		 :own (truevaluep (cl-bgg:item-own-of item-sxml))
		 :wanttobuy  (truevaluep (cl-bgg:item-wanttobuy-of item-sxml))
		 :wanttoplay (truevaluep (cl-bgg:item-wanttoplay-of item-sxml))
		 :rating (convert-rating-value (cl-bgg:item-rating-of item-sxml))
		 :comment ""))

@export
(defun update-collection (instance item-sxml)
  (setf (ownp instance) (truevaluep (cl-bgg:item-own-of item-sxml))
	(wanttobuyp instance) (truevaluep (cl-bgg:item-wanttobuy-of item-sxml))
	(wanttoplayp instance) (truevaluep (cl-bgg:item-wanttoplay-of item-sxml))
	(rating-of instance) (convert-rating-value (cl-bgg:item-rating-of item-sxml))))

@export
(defun push-collection-to-db (username item-sxml &key (db *bgg-db*))
  (when (and username item-sxml)
    (let* ((guid (cl-bgg:item-guid-of item-sxml))
	   (collection-instance (car (find-collection username guid))))
      (if collection-instance 
	  (update-collection collection-instance item-sxml)
	  (setf collection-instance (make-collection username item-sxml)))
      (with-transaction ()
	(update-records-from-instance collection-instance :database db)))))

@export
(defun push-collections-to-db (username collections-xml &key (db *bgg-db*))
  (when (and username collections-xml)
    (let* ((items (cl-bgg:items-of collections-xml)))
      (map nil 
	   #'(lambda (item)
	       (push-collection-to-db username item :db db)) items))))

@export
(defun push-guids-to-db (guids &key (db *bgg-db*))
  (let ((newgames (car (get-newgames))))
    (if newgames
	(setf (guids-of newgames) guids)
	(setf newgames (make-instance '<bgg-newgame>
				      :id 1
				      :guids guids)))
    (with-transaction ()
      (update-records-from-instance newgames :database db))))

@export
(defun update-collection-comment (username guid comment &key (db *bgg-db*))
  (when (and username guid comment)
    (whenbind collection (car (find-collection username guid :db db))
      (setf (comment-of collection) comment)
      (with-transaction ()
	(update-records-from-instance collection :database db)))))

@export
(defun push-boardgame-jpname-to-db (guid jpname &key (db *bgg-db*))
  (when (and guid jpname)
    (lt1 boardgame-jpname (car (find-boardgame-jpname guid :db db))
      (if boardgame-jpname
	  (setf (jpname-of boardgame-jpname) jpname)
	  (setf boardgame-jpname (make-bgg-boardgame-jpname guid jpname)))
      (with-transaction ()
	(update-records-from-instance boardgame-jpname :database db)))))

(locally-enable-sql-reader-syntax)
@export
(defun find-boardgame (guid &key (db *bgg-db*))
  (select '<bgg-boardgame>
	  :where [= [guid] guid]
	  :flatp t
	  :caching t
	  :database db))

@export
(defun find-collection (username guid &key (db *bgg-db*))
  (select '<bgg-collection>
	  :where [and [= [username] username] [= [guid] guid]]
	  :flatp t
	  :caching t
	  :database db))

@export
(defun find-collections (username &key (db *bgg-db*))
  (select '<bgg-collection>
	  :where [and [= [username] username]]
	  :order-by '(([rating] :desc))
	  :flatp t
	  :caching t
	  :database db))

@export
(defun find-own-collections (username &key (db *bgg-db*))
  (select '<bgg-collection>
	  :where [and [= [username] username] [= [own] T]]
	  :order-by '(([rating] :desc))
	  :flatp t
	  :caching t
	  :database db))

@export
(defun get-newgames (&key (db *bgg-db*))
  (select '<bgg-newgame>
	  :flatp t
	  :caching t
	  :database db))

@export
(defun find-boardgame-jpname (guid &key (db *bgg-db*))
  (select '<bgg-boardgame-jpname>
	  :where [= [guid] guid]
	  :flatp t
	  :caching t
	  :database db))
(restore-sql-reader-syntax-state)

@export 
(defun find-boardgame-for-year-published-view (&key (db *bgg-db*))
  (query "SELECT c.guid, g.name, j.jpname, g.yearpublished, g.thumbnail, g.boardgamedesigner, g.boardgamemechanic, g.boardgamefamily, c.rating , c.own , c.wanttobuy , c.wanttoplay
         from '<BGG_COLLECTION>' c INNER JOIN '<BGG_BOARDGAME>' g 
            ON c.guid = g.guid
         LEFT OUTER JOIN '<BGG_BOARDGAME_JPNAME>' j
            ON c.guid = j.guid ;" :database db))

@export
(defun find-one-boardgame-info (guid &key (db *bgg-db*))
  (lt1 query-string (format nil "SELECT c.guid, g.name, j.jpname, g.yearpublished, g.thumbnail , g.boardgamedesigner, c.rating
         from '<BGG_COLLECTION>' c INNER JOIN '<BGG_BOARDGAME>' g 
            ON c.guid = g.guid
         LEFT OUTER JOIN '<BGG_BOARDGAME_JPNAME>' j
            ON c.guid = j.guid
         WHERE c.guid=~d;" guid)
    (query query-string :database db)))

;(drop-table '<bgg-collection> :database *bgg-db*)

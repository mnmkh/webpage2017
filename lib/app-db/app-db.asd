(in-package :cl-user)

(defpackage :app-db-asd
  (:use :cl :asdf))

(in-package :app-db-asd)

(defsystem :app-db
    :description "app common database libs."
    :version "0.1"
    :author "mnmkh"
    :serial T
    :components ((:static-file "app-db.asd")
		 (:file "packages")
		 (:file "blog-db")
		 (:file "gamelist-db")
		 (:file "app-db"))
    :depends-on (:skip
		 :clsql
		 :clsql-sqlite3
		 :hu.dwim.defclass-star
		 :cl-bgg
		 :cl-annot))

(defsystem :app-db-test
  :components ((:module :t
                        :serial t
                        :components ((:file "packages"))))
  :depends-on (:app-db :fiveAM))


(defmethod perform ((op asdf:test-op) (c (eql (find-system 'app-db))))
  (load (merge-pathnames "t/run-test.lisp" (system-source-directory c))))
